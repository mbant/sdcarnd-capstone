# Udacity Self-Driving Car Enginerr Nanodegree: System Integration (Capstone) Project <!-- omit in toc -->

[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

This is the project repo for the final project of the Udacity Self-Driving Car Nanodegree: Programming a Real Self-Driving Car.

##  Table of Contents <!-- omit in toc -->
- [Project Overview](#project-overview)
- [ROS Implementation Details](#ros-implementation-details)
  - [Waypoints Updater](#waypoints-updater)
  - [Traffic Light Detection](#traffic-light-detection)
  - [DBW Controller](#dbw-controller)
- [Installation Instructions](#installation-instructions)
  - [Native Installation](#native-installation)
  - [Docker Installation](#docker-installation)
  - [Simulator Testing](#simulator-testing)
  - [Real World Testing](#real-world-testing)


Project Overview
---

Meet Carla, Udacity's self-driving car

![Carla](imgs/carla.png)

This is the repositpry for the final project of the Udacity Self-Driving Car Nanodegree; a challenging and rewarding experience where everything is tied together and we, as students, get to actually put our code on a real self-driving car 
and let Carla drive herself around the Udacity headquarter's designed lot (with traffic lights!).

The task at hand is to implement a set of [ROS](https://www.ros.org/) nodes that work together with Carla's infrastructure to allow the car to perceive its surroundings ([#Traffic-Light-Detection]), updates its path based on the environment 
([#Waypoints-Updater]) and implement controls such that it correctly follows that path.

This is not at the level of [Udacity's full self-driving success](https://www.youtube.com/watch?v=-j0tc0Y1CIE&app=desktop), and there are multiple part I'd love to thinker with, like abandoning the waypoints crutch and using Deep Learning or Computer Vision to [detect](https://gitlab.com/mbant/CarND/LaneLines-P1/README.md) [Lane Lines](https://gitlab.com/mbant/CarND/AdvLaneLines-p2/README.md) and using that information to provide Carla with a working [path planner](https://gitlab.com/mbant/CarND/Path-Planning-P6/README.md).
What can be found here nonetheless requires understading of the autonomous system at hand and of the ROS framework and was a fun and challenging problem; it contains various ROS nodes implementations amongst which a working example of a **deployed Machine Learning model** on an autonomous robotic system that uses python and ROS to interface the Tensorflow model and the `robot` itself. 
See [this README](./object_detection/README.md) for more details on this part.

I'd essentually like to go from here (which is the current implementation) -- > to here, a "full" self driving system!

<img src="./imgs/final-project-ros-graph-v2.png" alt="current_system.png" width="450"/>
<img src="./imgs/wow_graph.png" alt="wow_system.png" width="600"/>


ROS Implementation Details
---

### Waypoints Updater


After the `waypoint_loader` node is run and the `/base_waypoints` topic is received and acquired by the system, the `waypoints_updater` node is responsible of associating to each waypoint on the predetermined path a desired linear velocity
that allow for smooth but effective acceleations and, in case of obstacles, decelerations. This permit to the `waypoint_follower` to then generate the appropriate `Twist_Commands` for the DBW node to follow.

In the implementation we subset the waypoints in front of the current car position to a manageable and sensible size and update them based on messages received by both the current position and velocity of the car and the perceived obstacles on the way. 
In particular in the case of obstacles (mostly red traffic lights in this example) it is necessary to delerate smoothly the car and 
while the `waypoint_follower` is resposible for  genereting *safe* *jerk-minimizing* trajectories to maximize both safety and perceived comfort in the car, projecting a smoothly decreasing linear velocity on the following waypoints before the obstacle
is handled here. In order to improve on the suggested "square-root" deceleration function I implemented a monotonically decreasing `sigmoid` deceleration function relative to the distance to the stop line, that takes into accout
current velocity, vehicle mass and desired stop line to compute a reasonable braking distance and uses that to compute the flexes of the sigmoid function, so that velocity can be decreased in a jerk-minimizing way.

![braking](./imgs/braking.jpg)

While technically sound, because of the sort-of-loose interaction with the `waypoint_follower` node and with the subsequent commands given by the PID controller, the lower plateau of the deceleration function induces some weird behaviours
in the actual car, resulting in Carla usually (especially at low starting speed) stopping a few meters behind the actual stop line and then crawling to it. Without a more through revamp on the system, it turns out that the more 
"abrupt" stopping of the `sqrt` function profile produces smoother decelerations overall, without any bounce, and will thus be the preferred deceleration funcion.

### Traffic Light Detection

This was definitively the most interestnig (to me) and challenging part of the project. I decided to use a Deep Learning approach to solve the problem of detecting and classifying the traffic lights both on simulations and real world
and to leverage heavily transfer learning to produce meaningful results in a feasible time  with the available resources. [Turns out Facebook is a big fan of this approach as well](https://arxiv.org/abs/1805.00932).

I used a [Single Shot Detection](https://arxiv.org/abs/1512.02325) [MobileNet v1](https://arxiv.org/abs/1704.04861) architecture with [Pooling Pyramid Network](https://arxiv.org/abs/1807.03284), pretrained on the [COCO dataset](cocodataset.org) (note, all different part of the network seemingly complex name link to relevant literature on the subject and I suggest checking these resources out,
I always like to make informed choices); the network architecture was chosen because of efficiency reasons 
(I first tried a larger [Faster RCNN](https://arxiv.org/abs/1506.01497) network that worked perfectly on images but introduced too much latency in the processing of incoming images in the actual self-driving car) 
and also because of its good accuracy shown (via mAP) on the COCO dataset.

I used the [Tensorflow Object Detection API](https://github.com/tensorflow/models/blob/master/research/object_detection/README.md) to get the pretrained model, then fine-tuned it to fit the [Bosch Small Traffic Light Dataset](https://hci.iwr.uni-heidelberg.de/node/6132) mostly to evaluate its performances, and lastly I "hand-labelled" just over 2500 images
taken from both the simulator and the ROS bags provided by Udacity to fine-tune it again on our target data. You can find all the process in more details in the dedicated [`object_detection/README.md`](./obj_detection/README.md).

![carla_gif](./obj_detection/results/carla_small.gif)
![loop_gif](./obj_detection/results/loop_small.gif)

### DBW Controller

Carla, a 2016 Lincoln MKZ is equipped with Drive by Wire (DBW) technology, which makes possible controlling the throttle, steering and brakes electronically.
In particular, the DBW Node (in `dbw_node.py`) is resposible handling all the communications (from subscribing to the topics relative to the car current state and the desired states in the next waypoints,
 to publishing all throttle, steering and braking commands to the relevant topics) and the actual controls are implemented in the `twist_controller.py` file.

Steering targets are generated by the `YawController` class inside `yaw_controller.py`, while Throttle and Brake commands use separate PID controllers found in `pid.py`. 
These make also use of a provided low-pass filter to remove high frequency noise from the measured vehicle velocity and rely on the target velocity associated with each waypoint to generate the appropriate commands.
The parameters of these controllers were tuned empirically by the Udacity team to work on Carla and thus tweaking them is outsidethe scope of the current project (see for example [here](https://gitlab.com/mbant/CarND/PID-Controller-P7/README.md) to see how it can be done); 
if we were to remove the waypoints from the equation, a Deep Learning approach to generate these commands has been show to work before (see for example [here](https://github.com/udacity/self-driving-car/tree/master/steering-models) or even [here](https://gitlab.com/mbant/CarND/Behavioral-Cloning-P3/README.md)).

Of note in the current implementation is that brake and throttle commands are generated and published in separate branches of a conditional statement and reset when a competing signal is sent to the DWB node, and as such
are not allowed to interfere with one another and allowing smoother transitions between one or the other. 


Installation Instructions
---

### Native Installation

* Be sure that your workstation is running Ubuntu 16.04 Xenial Xerus or Ubuntu 14.04 Trusty Tahir. [Ubuntu downloads can be found here](https://www.ubuntu.com/download/desktop).
* If using a Virtual Machine to install Ubuntu, use the following configuration as minimum:
  * 2 CPU
  * 2 GB system memory
  * 25 GB of free hard drive space

  The Udacity provided virtual machine has ROS and Dataspeed DBW already installed, so you can skip the next two steps if you are using this.

* Follow these instructions to install ROS
  * [ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu) if you have Ubuntu 16.04.
  * [ROS Indigo](http://wiki.ros.org/indigo/Installation/Ubuntu) if you have Ubuntu 14.04.
* [Dataspeed DBW](https://bitbucket.org/DataspeedInc/dbw_mkz_ros)
  * Use this option to install the SDK on a workstation that already has ROS installed: [One Line SDK Install (binary)](https://bitbucket.org/DataspeedInc/dbw_mkz_ros/src/81e63fcc335d7b64139d7482017d6a97b405e250/ROS_SETUP.md?fileviewer=file-view-default)
* Download the [Udacity Simulator](https://github.com/udacity/CarND-Capstone/releases).

### Docker Installation
[Install Docker](https://docs.docker.com/engine/installation/)

Build the docker container
```bash
docker build . -t capstone
```

Run the docker file
```bash
docker run -p 4567:4567 -v $PWD:/capstone -v /tmp/log:/root/.ros/ --rm -it capstone
```

### Simulator Testing

1. Clone the project repository
```bash
git clone https://gitlab.com/mbant/sdcarnd-capstone
```

2. Install python dependencies
```bash
cd CarND-Capstone
pip install -r requirements.txt
```
3. Make and run styx
```bash
cd ros
catkin_make
source devel/setup.sh
roslaunch launch/styx.launch
```
4. Run the simulator

<img src="./imgs/smooth.png" alt="smooth straight" width="400"/>
<img src="./imgs/smooth_curve.png" alt="smooth curve" width="400"/>
<img src="./imgs/smooth_stop.png" alt="smooth stop" width="400"/>

### Real World Testing

In order to test that the detection works on real-life images, you might want to test on the [training bag](https://drive.google.com/file/d/0B2_h37bMVw3iYkdJT1RSUlJIamM/view?usp=sharing) that was recorded on the Udacity self-driving car.

1. Download the bag

2. Unzip the file
```bash
unzip traffic_loop_bag_file.zip
```
3. Launch your project in site mode
```bash
cd CarND-Capstone/ros
roslaunch launch/site.launch
```
4. Run 
```bash
rosrun image_proc image_proc
```
to expose the `/image_color` ros topic (otherwise only the `/image_raw` is available, but it might not wrk with your classfier)

6. Play the bag file
```bash
rosbag play -l traffic_loop_bag_file/loop_with_traffic_light.bag
```
7. Run `rviz` to visualize what's happening (use [this](./default.rviz) config file)

8. Confirm that traffic light detection works by using *e.g.* `rospy.loginfo()`.

<img src="./imgs/turning_red.png" alt="turning red" width="450"/>
<img src="./imgs/green.png" alt="green" width="450"/>
