#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped
from styx_msgs.msg import Lane, Waypoint
from std_msgs.msg import Int32
import numpy as np
from scipy.spatial import KDTree

import math

'''
This node will publish waypoints from the car's current position to some `x` distance ahead.

As mentioned in the doc, you should ideally first implement a version which does not care
about traffic lights or obstacles.

Once you have created dbw_node, you will update this node to use the status of traffic lights too.

Please note that our simulator also provides the exact location of traffic lights and their
current status in `/vehicle/traffic_lights` message. You can use this message to build this node
as well as to verify your TL classifier.

'''

LOOKAHEAD_WPS = 50 # Number of waypoints we will publish
MAX_DECEL = .5 # from walkthrough

class WaypointUpdater(object):
    def __init__(self):
        rospy.init_node('waypoint_updater')

        rospy.Subscriber('/current_pose', PoseStamped, self.pose_cb)
        self.base_waypoints_sub = rospy.Subscriber('/base_waypoints', Lane, self.waypoints_cb)

        # TODO: Add a subscriber for /traffic_waypoint and /obstacle_waypoint below
        rospy.Subscriber('/traffic_waypoint', Int32, self.traffic_cb)
        # rospy.Subscriber('/obstacle_waypoint', Lane, self.obstacle_cb)


        self.final_waypoints_pub = rospy.Publisher('/final_waypoints', Lane, queue_size=1)

        # TODO: Add other member variables you need below
        self.pose = None
        self.base_lane = None
        self.waypoints_2d = None
        self.waypoint_tree = None

        self.stopline_wp_index = -1

        self.max_velocity = None
        self.mass = None
        self.braking_distance = None
        self.sig_flex = None

        self.loop()

    def loop(self):
        rate = rospy.Rate(50)
        while not rospy.is_shutdown():
            if self.pose and self.base_lane:
                self.publish_waypoints()
            rate.sleep()


    def get_closest_waypoint(self):
        x = self.pose.pose.position.x #rosmsg info to get the number of "poses"
        y = self.pose.pose.position.y

        closest_id = self.waypoint_tree.query([x,y],1)[1] # element 0 is the position, we need the index
        # check if behind or in front
        closest_coord = self.waypoints_2d[closest_id]
        prev_coord =  self.waypoints_2d[closest_id-1]

        # compute dot product to check if in front
        cl_vect = np.array(closest_coord)
        prev_vect = np.array(prev_coord)
        pos_vect = np.array([x,y]) #us

        val = np.dot(cl_vect-prev_vect,pos_vect-cl_vect)
        # val contains the dot product of the vectors prev->closest and closest->us
        # so if positive (both have the same direction) we'd be already in front of closest

        if val > 0: # if it's behind
            closest_id = (closest_id+1) % len(self.waypoints_2d) # take next one modulo the length of the vector
        
        return closest_id

    def publish_waypoints(self):
        final_lane = self.generate_lane()
        self.final_waypoints_pub.publish(final_lane)
    
    def generate_lane(self):
        lane = Lane()
        #get closest waypoint index
        closest_id = self.get_closest_waypoint()
        farthest_id = closest_id + LOOKAHEAD_WPS
        base_waypoints = self.base_lane.waypoints[closest_id:farthest_id]

        if self.stopline_wp_index == -1 or (self.stopline_wp_index >= farthest_id):
            lane.waypoints = base_waypoints
        else:
            lane.waypoints = self.decelerate_waypoints(base_waypoints,closest_id)

        return lane

    def get_sigmoid_vel(self,d):
        if self.max_velocity:
            """
            We essentially compute a scaled sigmoid function 
            that has flexes close to the half of the braking_distance
            and start decreasing from an asymptote at the max_velocity
            """
            return self.max_velocity/(1.+math.exp(-self.sig_flex*(d-self.braking_distance*.4)))
        else:
            return math.sqrt(2 * MAX_DECEL * d) # fallback function

    def decelerate_waypoints(self,waypoints,closest_index):
        temp = []
        stop_index = max(self.stopline_wp_index - closest_index - 2 , 0) ## 2 waypoints before the stop line we want the car to come to a stop

        for i,wp in enumerate(waypoints):
            p = Waypoint()
            p.pose = wp.pose
            dist = self.distance(waypoints,i,stop_index) ## compute distance from the stop_index
            # compute the desired velocity at that waypoint and the append it to lane

            vel_sqrt = math.sqrt(2 * MAX_DECEL * dist) ## maybe smooth this out ?

            # vel_sig = self.get_sigmoid_vel(dist)
            # if i == 0 :
                # rospy.logwarn('\ndist= {3}\ncurr_vel= {0}\nvel_sqrt= {1}\n vel_sig= {2}\n\n'.format(wp.twist.twist.linear.x,vel_old,vel,dist))
            
            vel = vel_sqrt #
            if vel < 1. or ((self.stopline_wp_index >= i) and (i >= stop_index)):
                vel = 0

            p.twist.twist.linear.x = min(vel,wp.twist.twist.linear.x)
            temp.append(p)
        
        return temp


    def pose_cb(self, msg):
        self.pose = msg

    def waypoints_cb(self, waypoints):
        self.base_lane = waypoints
        self.max_velocity = ( rospy.get_param('~/waypoint_loader/velocity') * 1000.) / (60. * 60.)
        self.mass = rospy.get_param('~vehicle_mass', 1736.35) 

        # stopping distance is proportional to the square of the speed of the vehicle times its mass
        self.braking_distance = self.max_velocity**2 * self.mass * 0.0005 # the constant is found from literaturefor clear weather and nice grip on road
        self.sig_flex = 5./self.braking_distance*math.log(2.+math.sqrt(3))

        # rospy.logwarn('{0}, {1}'.format(self.braking_distance,self.sig_flex))

        if not self.waypoints_2d:
            # get them in a 2d list type using list comprehension and used KDTree to efficiently lookup the closest waypoints
            self.waypoints_2d = [[wp.pose.pose.position.x, wp.pose.pose.position.y] for wp in self.base_lane.waypoints]
            self.waypoint_tree = KDTree(self.waypoints_2d)

        # Unsubscribe now as these get published just once anyway and we're done processing
        self.base_waypoints_sub.unregister()
        rospy.loginfo("Base waypoints obtained and processed, we can start the car!")


    def traffic_cb(self, msg):
        # rospy.logwarn('traffic topic -> {0}'.format(msg.data))
        self.stopline_wp_index = msg.data

    def obstacle_cb(self, msg):
        # TODO: Callback for /obstacle_waypoint message. We will implement it later
        pass

    def get_waypoint_velocity(self, waypoint):
        return waypoint.twist.twist.linear.x

    def set_waypoint_velocity(self, waypoints, waypoint, velocity):
        waypoints[waypoint].twist.twist.linear.x = velocity

    def distance(self, waypoints, wp1, wp2):
        dist = 0
        dl = lambda a, b: math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2  + (a.z-b.z)**2)
        for i in range(wp1, wp2+1):
            dist += dl(waypoints[wp1].pose.pose.position, waypoints[i].pose.pose.position)
            wp1 = i
        return dist


if __name__ == '__main__':
    try:
        WaypointUpdater()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start waypoint updater node.')
