from styx_msgs.msg import TrafficLight
import tensorflow as tf
import cv2
import rospy
import datetime
import numpy as np
import os
import time

class TLClassifier(object):
    def __init__(self, h, w, is_site):
        self.image_size = (h,w)
        self.threshold=0.5
        self.elapsed_time=0
        self.n_classification=0

        self.class_map = {
            1: TrafficLight.GREEN,
            2: TrafficLight.YELLOW,
            3: TrafficLight.RED
        } # from the labels_map.pbtxt

        # Load the correct inference graph
        if is_site:
            self.load_inference_graph('site_graph.pb')
        else:
            self.load_inference_graph('sim_graph.pb')

        self.init_model()

    def load_inference_graph(self, graph_file):
        base_folder = os.path.dirname(os.path.realpath(__file__))
        graph_path = os.path.join(base_folder, graph_file)

        rospy.loginfo('Loading frozen graph: %s', graph_path)

        graph = tf.Graph()

        with graph.as_default():
            graph_def = tf.GraphDef()
            with tf.gfile.GFile(graph_path, 'rb') as fid:
                serialized_graph = fid.read()
                graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(graph_def, name='')

        self.sess = tf.Session(graph = graph)

        self.image_tensor = graph.get_tensor_by_name('image_tensor:0')
        self.boxes_tensor = graph.get_tensor_by_name('detection_boxes:0')
        self.scores_tensor = graph.get_tensor_by_name('detection_scores:0')
        self.classes_tensor = graph.get_tensor_by_name('detection_classes:0')
        self.detections_tensor = graph.get_tensor_by_name('num_detections:0')

    def init_model(self):
        '''
        Run inference on an empty image, to allow cuda libraries to load and GPU memory to be reserved,
        so that every inference run subsequently has the hardware ready to use
        '''
        image = np.zeros((self.image_size[0], self.image_size[1], 3), dtype=np.uint8)
        image = np.expand_dims(image, axis=0)
        
        s_time = time.time()
        ops = [self.detections_tensor, self.boxes_tensor, self.scores_tensor, self.classes_tensor]
        _, _, _, _ = self.sess.run(ops, feed_dict = { self.image_tensor : image })
        e_time = time.time() - s_time

        rospy.loginfo('Tensorflow classifier init completed (Time elapsed: %.3f ms)', e_time * 1000. )

    def get_classification(self, image_color):
        '''
	    Determines the color of the traffic light in the image
        '''

        image_xpanded = np.expand_dims(image_color, axis=0)

        s_time = time.time()

        ops = [self.detections_tensor, self.boxes_tensor, self.scores_tensor, self.classes_tensor]
        _, _, detection_scores, detection_classes = self.sess.run(ops, feed_dict = { self.image_tensor : image_xpanded })

        e_time = time.time() - s_time

        detection_scores = detection_scores[0]
        detection_classes = detection_classes[0].astype(np.uint8)

        light = TrafficLight.UNKNOWN
        if detection_scores[0] > self.threshold: #if the best class is above the threshold
            light = self.class_map.get(detection_classes[0], TrafficLight.UNKNOWN) #get its class instead

        self.elapsed_time += e_time * 1000.
        self.n_classification += 1
 
        ''' # DEBUG
        if self.n_classification % 50 == 0:
            rospy.loginfo('Classification # %s, Avg Classification Time: %.3f ms', self.n_classification, self.elapsed_time / self.n_classification)
        class_lab = {
            1: 'Green',
            2: 'Yellow',
            3: 'Red'
        } # from the labels_map.pbtxt
        rospy.loginfo('Classified as: {0}'.format(class_lab.get(detection_classes[0], 'No Traffic Light')))
        '''
        
        return light
