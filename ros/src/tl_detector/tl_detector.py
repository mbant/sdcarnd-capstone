#!/usr/bin/env python
import rospy
from std_msgs.msg import Int32
from geometry_msgs.msg import PoseStamped, Pose
from styx_msgs.msg import TrafficLightArray, TrafficLight
from styx_msgs.msg import Lane
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from light_classification.tl_classifier import TLClassifier
from scipy.spatial import KDTree
import tf
import cv2
import yaml

USE_STATE_INFORMATION = False
STATE_COUNT_THRESHOLD = 2

class TLDetector(object):
    def __init__(self):
        rospy.init_node('tl_detector')

        config_string = rospy.get_param("/traffic_light_config")
        self.config = yaml.load(config_string)

        self.pose = None
        self.waypoints = None
        self.waypoints_2d = None
        self.camera_image = None
        self.lights = []

        ## skip some images to alleviate the burden of the classifier
        self.n_skip_images = 2
        # if self.config['is_site']:
        #     self.n_skip_images -= 2 # negative is ok in case this is set to 0 ...
        self.image_count = 0
        # self.filenumber = 1
        
        self.base_waypoints_sub = rospy.Subscriber('/base_waypoints', Lane, self.waypoints_cb)
        rospy.Subscriber('/vehicle/traffic_lights', TrafficLightArray, self.traffic_cb)
        
        ## Sync current_pose and (if applicable, image_color)
        rospy.Subscriber('/current_pose', PoseStamped, self.pose_cb)

        '''
        /vehicle/traffic_lights provides you with the location of the traffic light in 3D map space and
        helps you acquire an accurate ground truth data source for the traffic light
        classifier by sending the current color state of all traffic lights in the
        simulator. When testing on the vehicle, the color state will not be available. You'll need to
        rely on the position of the light and the camera image to predict it.
        '''
        self.classifier = None

        if USE_STATE_INFORMATION and not self.config['is_site']:
            rospy.Subscriber('/image_color', Image, self.image_cb)
            # using only lights' state informations
            pass
        else:
            rospy.Subscriber('/image_color', Image, self.image_cb)
            self.classifier = TLClassifier(self.config['camera_info']['image_height'], self.config['camera_info']['image_width'],self.config['is_site'])


        self.upcoming_red_light_pub = rospy.Publisher('/traffic_waypoint', Int32, queue_size=1)
        self.bridge = CvBridge()
        self.listener = tf.TransformListener()

        self.state = TrafficLight.UNKNOWN
        self.last_state = TrafficLight.UNKNOWN
        self.last_wp = -1
        self.state_count = 0

        rospy.spin()

    def pose_cb(self, msg):
        self.pose = msg

    def waypoints_cb(self, waypoints):
        self.waypoints = waypoints
        if not self.waypoints_2d:
            # get them in a 2d list type using list comprehension and used KDTree to efficiently lookup the closest waypoints
            self.waypoints_2d = [[wp.pose.pose.position.x, wp.pose.pose.position.y] for wp in waypoints.waypoints]
            self.waypoint_tree = KDTree(self.waypoints_2d)

        # Unsubscribe now as these get published just once anyway and we're done processing
        self.base_waypoints_sub.unregister()
        # rospy.loginfo("Base waypoints obtained and processed, we can start the car!")

    def traffic_cb(self, msg):
        self.lights = msg.lights

    def image_cb(self, msg):
        """Identifies red lights in the incoming camera image and publishes the index
            of the waypoint closest to the red light's stop line to /traffic_waypoint

        Args:
            msg (Image): image from car-mounted camera

        """

        ''' Produce images for training ''
        cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        cv2.imwrite('/home/marco/simulator_images/image'+str(self.filenumber)+'.jpg',cv_image)
        rospy.loginfo('image '+ str(self.filenumber)+' written')
        self.filenumber += 1
        '' END Produce images for training '''

        # rospy.loginfo('Received img')
        
        ## skip some images
        if self.image_count < self.n_skip_images:
            self.image_count += 1
            
        # then start the real work    
        else:
            self.image_count = 0

            self.camera_image = msg

            light_wp, state = self.process_traffic_lights()
            # rospy.loginfo('Classified wp {1} as {0}'.format(state,light_wp))

            ''' Publish upcoming red lights at camera frequency.
            Each predicted state has to occur `STATE_COUNT_THRESHOLD` number
            of times till we start using it. Otherwise the previous stable state is used.
            '''

            if self.state != state:

                self.state_count = 0
                self.state = state
                # rospy.logwarn('Changing State')

            elif self.state_count >= STATE_COUNT_THRESHOLD:

                self.last_state = self.state
                light_wp = light_wp if state == TrafficLight.RED else -1
                self.last_wp = light_wp
                self.upcoming_red_light_pub.publish(Int32(light_wp))
                # rospy.loginfo('Published {0}   ----   SC= {1}'.format(self.state,self.state_count))

            else:
                # keep publishing what you saw before
                self.upcoming_red_light_pub.publish(Int32(self.last_wp))
                # rospy.loginfo('RE-Published {0}   ----   SC= {1}'.format(self.last_wp,self.state_count))

            self.state_count += 1


    def get_closest_waypoint(self, x, y):
        '''Identifies the closest path waypoint to the given position
            https://en.wikipedia.org/wiki/Closest_pair_of_points_problem
        Args:
            pose (Pose): position to match a waypoint to

        Returns:
            int: index of the closest waypoint in self.waypoints

        '''
        closest_id = self.waypoint_tree.query([x,y],1)[1]
        return closest_id

    def get_light_state(self, light):
        '''Determines the current color of the traffic light

        Args:
            light (TrafficLight): light to classify

        Returns:
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)
        '''
        if USE_STATE_INFORMATION and not self.config['is_site']:
            light_state = light.state # for testing purposes
        else:
            cv_image = self.bridge.imgmsg_to_cv2(self.camera_image, "rgb8")
            light_state =  self.classifier.get_classification(cv_image)

        return light_state

    def process_traffic_lights(self):
        """Finds closest visible traffic light, if one exists, and determines its
            location and color

        Returns:
            int: index of waypoint closes to the upcoming stop line for a traffic light (-1 if none exists)
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)

        """
        closest_light = None
        stopline_wp_index = None

        # List of positions that correspond to the line to stop in front of for a given intersection
        stop_line_positions = self.config['stop_line_positions']
        car_wp_index = self.get_closest_waypoint(self.pose.pose.position.x,self.pose.pose.position.y)

        #Find the closest visible traffic light (if one exists)
        diff = len(self.waypoints.waypoints)
        for i, light in enumerate(self.lights):
            # get the stop line position
            line = stop_line_positions[i]
            temp_wp_index = self.get_closest_waypoint(line[0],line[1])
            # get closest waypoint index
            d = temp_wp_index - car_wp_index
            if d >= 0 and d < diff:
                diff = d
                closest_light = light
                stopline_wp_index = temp_wp_index

        if closest_light:
            state = self.get_light_state(closest_light)
            return stopline_wp_index, state

        return -1, TrafficLight.UNKNOWN

if __name__ == '__main__':
    try:
        TLDetector()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start traffic node.')

