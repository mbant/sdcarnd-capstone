from pid import PID
from lowpass import LowPassFilter
from yaw_controller import YawController
import rospy

GAS_DENSITY = 2.858
ONE_MPH = 0.44704


class Controller(object):
    def __init__(self, vehicle_mass, fuel_capacity, brake_deadband, 
                    decel_limit, accel_limit, wheel_radius, wheel_base, 
                    steer_ratio, max_lat_accel, max_steer_angle):
        
        self.yaw_controller = YawController(wheel_base, steer_ratio,
                    0.1, max_lat_accel, max_steer_angle) 
        # these are all provided by dbw_node except the min_speed=0.1 which is given in the walkthrough as acomfort parameter

        # experimental parameters suggested by walktrhough
        kp = 0.3
        ki = 0.1
        kd = 0.02
        # comfort parameters
        mn = 0. # minimum throttle
        mx = 0.2 # maximum throttle 
        self.throttle_controller = PID(kp,ki,kd,mn,mx)

        # to control the noisy messages in current_velocity via a provided low pass filter
        tau = 0.5
        ts = 0.02
        self.velocity_lpf = LowPassFilter(tau, ts)

        # store some of the other parameters on init
        self.vehicle_mass=vehicle_mass
        self.brake_deadband=brake_deadband
        self.decel_limit=decel_limit
        self.accel_limit=accel_limit
        self.wheel_radius=wheel_radius

        # useful for timing
        self.last_time = rospy.get_time()


    def control(self, current_vel, dbw_enabled, linear_vel, angular_vel):

        # check if car is in manual mode
        if not dbw_enabled:
            self.throttle_controller.reset() 
                # disconnect the PID controllet to avoid error accumulating for the Integral term
                # to avoid erratic behaviours at re-insertion of the controller
            return 0., 0., 0.
        
        # get current velocity
        current_vel = self.velocity_lpf.filt(current_vel)

        ## get the comands
        steer = self.yaw_controller.get_steering(linear_vel,angular_vel,current_vel)

        velocity_error = linear_vel-current_vel
        self.last_velocity = current_vel

        ## update internal time
        current_time = rospy.get_time()
        sample_time = current_time - self.last_time
        self.last_time = current_time

        # get throttle
        throttle = self.throttle_controller.step(velocity_error, sample_time)

        # get brake
        brake = 0
        if linear_vel == 0 and current_vel < 0.1: # 0.1 m/s is the minimum velocity
            throttle = 0
            brake = 700 # N*m - amount necessary to hold the car in place while stopped
                # like if we're waiting at a traffic light
                # tha corresponding acceleration is around 1m/s^2
        elif throttle < 0.1 and velocity_error < 0.: #if we're stopping/decelerating
            throttle = 0
            decel = max(velocity_error,self.decel_limit)
            brake = abs(decel) * self.vehicle_mass * self.wheel_radius # Torque N*m

        """
        rospy.logwarn('Current velocity = {0}'.format(current_vel))        
        rospy.logwarn('Target velocity = {0}'.format(linear_vel))        
        rospy.logwarn('Current velocity = {0}'.format(current_vel))        
        rospy.logwarn('Current velocity = {0}'.format(current_vel))        
        
        rospy.logwarn('Out throttle = {0}'.format(throttle))        
        rospy.logwarn('Out brake = {0}'.format(brake))        
        rospy.logwarn('Out steer = {0}'.format(steer))        
        """

        return throttle, brake, steer
