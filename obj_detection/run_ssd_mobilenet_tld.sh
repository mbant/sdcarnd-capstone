## Environment
TF_PATH="YOUR_PATH_TO_TENSORFLOW/models/research"
export PYTHONPATH=$PYTHONPATH:$TF_PATH:$TF_PATH/slim
export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}


TF_OBJ_D_PATH="YOUR_PATH_TO/tensorflow/models/research/object_detection"
PIPELINE_CONFIG_PATH="YOUT_PATH_TO_PIPELINE.config"
MODEL_DIR="OUT_PATH_MODELDIR"
NUM_TRAIN_STEPS=200000 # Change this if necessary

python3 ${TF_OBJ_D_PATH}/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --alsologtostderr

