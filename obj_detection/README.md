# Traffic light Detection using Tensorflow Object Detection API

## Prerequisites

First of all, clone and install the necessary prerequisites:
* Clone the [TF Models repository](https://github.com/tensorflow/models)
* Download and install TF Object Detection API as detailed in [the original repo](https://github.com/tensorflow/models/tree/master/research/object_detection)'s [`install.md`](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md)
* Test everything works using the `object_detection_tutorial.ipynb` in your newly cloned repo (you might need to update yur python environment)

In the meantime, download and extract the [Bosch Small Traffic Lights Dataset](https://github.com/bosch-ros-pkg/bstld).

Note that we will use both `python3` (as needed for the bstld repo's scripts) and `python2` which is needed for the CarND Capstone project.
Because of this you might need to re-build the pycocotools in order to use the COCO evaluation metris (if defined in the config below): see [this issue](https://github.com/cocodataset/cocoapi/issues/90).

## Testing on the Bosch Small Traffic Lights Dataset

Now as detailed in the [bstld repository](https://github.com/bosch-ros-pkg/bstld/tree/master/tf_object_detection) under `tf_object_detection`, run

`python PATH_TO_BSTLD_TF_OBJECT_DETECTION/to_tfrecords.py` with the following arguments, as specified by the help

    --train_yaml TRAIN_YAML
                          Path to train.yaml
    --test_yaml TEST_YAML
                          Path to test.yaml
    --additional_yaml ADDITIONAL_YAML
                          Path to train_additional.yaml
    --dataset_folder DATASET_FOLDER
                          Path to dataset folder
    --train_tfrecord TRAIN_TFRECORD
                          Path to train.tfrecord
    --valid_tfrecord VALID_TFRECORD
                          Path to valid.tfrecord
    --test_tfrecord TEST_TFRECORD
                          Path to test.tfrecord

This will create `tfrecord` objects for train and validation dataset. The script fails to create a test dataset for now.
Move the created files alongside the `bstld/tf_object_detection/label_maps/bstld_label_map.pbtxt` file to your working directory.

Now get a model from the [TF Model Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md). 
My choice for now will be the `Single Shot Detection (SSD) with Mobilenet v1 and PPN feature extractor` model, pre-trained on COCO:

    wget http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync_2018_07_03.tar.gz
    tar -zxvf ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync_2018_07_03.tar.gz

I chose this network because of the shown performances on the COCO dataset in terms especially of Accuracy (mAP) versus speed, as we need a fast object detection on the car.

Now we need to figure out a valid configuration file for our problem; to do that we can have a look both to [the official instructions](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/configuring_jobs.md) and to some [sample configurations](https://github.com/tensorflow/models/blob/master/research/object_detection/samples/configs/ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync.config) (also from the [bstld github repository](https://github.com/bosch-ros-pkg/bstld/tree/master/tf_object_detection/configs/) ) and adapt them to our local path and desired labels.

You can see my resulting config file in the `configs/ssd_mobilenet_v1_ppn_bstld.config` file, you should search for `CONFIGURE_YOUR_OWN` to be sure you're configuring the correct paths for your system. It is of particular importance the parameter 'num_classes: 4', as that is the number of classes for the 'bstld' dataset ('Off', 'Green', 'Yellow', 'Red').

Finally, create a `run.sh` script for convenience containing the following:

    TF_OBJ_D_PATH="YOUR_PATH_TO tensorflow/models/research/object_detection"
    PIPELINE_CONFIG_PATH="YOUR_PATH_TO model.config"
    MODEL_DIR="YOUR_PATH_TO model_dir (where you want to store YOURS ckpt)"
    NUM_TRAIN_STEPS=200000 # Change this if necessary
    SAMPLE_1_OF_N_EVAL_EXAMPLES=1
    
    python3 ${TF_OBJ_D_PATH}/model_main.py \
        --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
        --model_dir=${MODEL_DIR} \
        --num_train_steps=${NUM_TRAIN_STEPS} \
        --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
        --alsologtostderr

note that `MODEL_DIR` needs to point to a (likely new) directory where you want your trainig to be saved. If you use the directory of the previously stored checkpoint, the one that comes with the downloaded model and that you set in `fine_tune_checkpoint` in your `.config` file, you'll risk overwriting the stored checkpoint (or more likely you'll get a warning like `Skipping training since max_steps has already saved` if `num_train_steps`  is set to a number below the one used to produce the downloaded checkpoint).

While this is running you can track its progresses using

`tensorboard --logdir=YOUR_PATH_TO/model_dir`

I ended up training the model for about '30000' iterations before stopping.
The obtained results were satisfatory on test images from the `bstld`, but because Carla is different from the car used in that dataset and especially the camera is at a different angle and without other cars in the way, 
I knew I needed to train further on different data.

Note: to test your model you need to freeze your net using a tool provided by the TF Object Detection API [here](https://github.com/tensorflow/models/blob/master/research/object_detection/export_inference_graph.py), running it as

    python3 export_inference_graph.py     --input_type image_tensor     --pipeline_config_path YOUR_PATH_TO/ssd_mobilenet_v1_ppn_tld.config  \
        --trained_checkpoint_prefix YOUR_PATH_TO/ssd_mobilenet_v1_ppn_tld/model.ckpt-YOUR_CKPT     --output_directory YOUR_PATH_TO_OUTPUT

This creates a frozen graph you can play around with in the ['object_detection_tutorial.ipynb'](https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb).

## Further fine-tuning on Carla and the Udacity Simulator

Because of course, in the same way the `bstld` is different from Carla's data, the simulator produces very different images from the ones taken on the real car (traffic lights are in a different shape/color and position too!) 
I decided I needed data from both the Unity simulator and the real car but especially that I'd need to train two different models to adapt to these two different type of data.

I hence went on to train further on both a [dataset provided by Udacity](https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/traffic_light_bag_file.zip) in the form of a ROS bag taken from Carla and a complete loop of Carla (downloadable [here](https://drive.google.com/file/d/0B2_h37bMVw3iYkdJTlRSUlJIamM/view?usp=sharing) ), plus some images from the [Udacity simulator](https://github.com/udacity/CarND-Capstone/releases) as well.

I extracted the simulator's images directly by using `cv2.imwrite` inside the `image_cb` function while driving manually, while to extract the ROS bags I used the following ROS launch files

```
<launch>
  <node pkg="rosbag" type="play" name="rosbag" required="true" args="PATH_TO/traffic_light_training.bag"/>
  <node name="extract" pkg="image_view" type="extract_images" respawn="false" required="true" output="screen" cwd="ROS_HOME">
    <remap from="image" to="/image_color"/>
  </node>
</launch>
```

and 

```
<launch>
  <node pkg="rosbag" type="play" name="rosbag" required="true" args="/home/marco/traffic_loop_carla/loop_with_traffic_light.bag"/>
  <node name="image_proc" pkg="image_proc" type="image_proc"/>
  <node name="extract" pkg="image_view" type="extract_images" respawn="false" required="true" output="screen" cwd="ROS_HOME">
    <remap from="image" to="/image_raw"/>
    <remap from="/image_raw" to="/image_color"/>
  </node>
</launch>
```

(we need the `image_proc` package and the extra `<remap from="/image_raw" to="/image_color"/>` to expose the `\image_color` topic or only the raw images will be extrated in the loop).

The resulting datasets are around 1500 real images and a bit less than a 1000 for the simulated one, and I reserved some simulated ones  for validation later on.
I'll describe the process I went through for training on the real images, but essentially the same applies to the simulated data.

First, I used my pre-trained model to help me label the images, leveraging the [official tutorial](https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb) to produce bounding boxes on each image; 
I modified the `run_inference_for_single_image` function into the [`label_images.py`](label_images.py) script that uses `lxml` to produce the corresponding `XML` file containing my bounding boxes. 
(note that the detection threshold is explicitly set very low, I'd rather have false positives in this case).

Sadly, but not unexpectedly, this doesn't work out-of-the-box (otherwise I wound't need to re-train) and thus this led to a couple tedious days annotating,
following the official [docs](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md), 
the misslabeld images using [labelImg](https://github.com/tzutalin/labelImg) and veryfing/updating my XML.

<img src="../imgs/labeling_1.png" alt="labels1" width="350"/>
<img src="../imgs/labeling_3.png" alt="labels2" width="350"/>
<img src="../imgs/labeling_carla.png" alt="labelsreal" width="350"/>
<img src="../imgs/wait_what.png" alt="labelsreal" width="350"/>

From there I used the `Bosch STLD` utility `to_tfrecords.py` again, slightly modifying it to use `XML` instead of `yaml` to produce the tfreconds of my dataset.

For the simulated images I ended up annotating using the `labelImg` tool around 50 of them (30 red, 10 green and 10 yellow) and train a network on thos only, then use that to pre-label the remaining ~900 and finally re-checking with `labelImg`.
This worked pretty well so I didn't have to annotate them all manually, since the network pre-trained on `bstld` wasn't performing that well on the simulated images.

The rest follows similarly to before as well; I wrote a config file ( `configs/ssd_mobilenet_v1_ppn_tld.config` where `tld` now stands for a generic `traffic light data`) 
chaging `num_classes` to 3 (as we don't have `Off` anymore), changed the actual number of eval data points, and the relative paths in the file to point to the new labels and tfreconds. 
Most importantly I changed the `finetune_checkpoint` to point to the final `bstld`-trained model.

Finally, after creating the new `labels.pbtxt` file (in accordance with my `label_images.py` script) as

    item {
    id: 1
    name: 'Green'
    }

    item {
    id: 2
    name: 'Yellow'
    }

    item {
    id: 3
    name: 'Red'
    }


I run again the training, with the `model_main.py` script, for about 10000 more iterations (a few hours on the [Udacity Workspace](https://engineering.udacity.com/creating-a-gpu-enhanced-virtual-desktop-for-udacity-497bdd91a505)) (and on my local machine for the simulated data,
and this are the results (after exporting the graph as explained above).

<img src="./results/sim_empty.png" alt="sim_empty.png" width="200"/>
<img src="./results/sim_green.png" alt="sim_green.png" width="200"/>
<img src="./results/sim_yellow.png" alt="sim_yellow.png" width="200"/>
<img src="./results/sim_red.png" alt="sim_red.png" width="200"/>
<br>

<img src="./results/carla_red.png" alt="carla_red.png" width="200"/>
<img src="./results/carla_red2.png" alt="carla_red2.png" width="200"/>
<img src="./results/carla_red3.png" alt="carla_red3.png" width="200"/>
<img src="./results/carla_red4.png" alt="carla_red4.png" width="200"/>
<br>


<img src="./results/carla_empty.png" alt="carla_empty.png" width="200"/>
<img src="./results/carla_green.png" alt="carla_green.png" width="200"/>
<img src="./results/carla_yellow.png" alt="carla_yellow.png" width="200"/>
<br>

![carla_gif](./results/carla_small.gif)
![loop_gif](./results/loop_small.gif)
![sim_gif](./results/sim_small.gif)


## Exporting your trained classifier to a compatible TF version

Carla has TF 1.3 installed, which is sadly one version BEFORE the Tensorflow Object Detection API officially supported (and were created), but we need nonwtheless to try our best to make our inference model compatible with the car's software and libraries
to be sure we can run our code seemlessly. TF 1.4 models should be backward compatible with 1.3 though, so let's use that.

First, install a vitual environment to avoid messing with your local packages

```bash
python2.7 -m pip install virtualenv
virtualenv --python=python2.7 tensorflow1.4
cd tensorflow1.4/
source bin/activate
```
Install the necessary dependecies
```bash
python2.7 -m pip install pillow lxml matplotlib tensorflow==1.4 
```

Now clone the `tensorflow/models` github repository, but revert it back to a past commit

```bash
git clone https://github.com/tensorflow/models.git
cd models
git checkout d135ed9c04bc9c60ea58f493559e60bc7673beb7
```

Now create a subdirectory for the exporter and copy the necessary files (I'll clarify why we need this in a moment)

```bash
mkdir exporter
cp -r research/object_detection exporter/object_detection
cp -r research/slim exporter/slim
cd exporter
```

Now we need [protobuf](https://github.com/protocolbuffers)

```bash
mkdir proto
cd proto

wget https://github.com/protocolbuffers/protobuf/releases/download/v3.9.1/protoc-3.9.1-linux-x86_64.zip
unzip protoc-3.9.1-linux-x86_64.zip
cd ..
```

But before we install them, we have a problem! The model I used is a fairly recent one, and thus not all proto definitions (located in `object_detection/protoc/`) are available in the older version of the repo!
If, like me, you're using an SSD model then this amounts to going to the [current version of the repo](https://github.com/tensorflow/models),
navigate to `https://github.com/tensorflow/models/research/object_detection/protos/ssd.proto` and copy its content in the file with same name in your local (dated) repo.

You're now ready to run

```bash
./proto/bin/protoc object_detection/protos/*.proto --python_out=.
```

to compile those definition.
Now run

```bash
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
python2.7 object_detection/builders/model_builder_test.py
```
and if it returns `OK` then you're ready to export your TF1.4 compatible `frozen_inference_graph` using the command below:

```bash
python2.7 object_detection/export_inference_graph.py --input_type=image_tensor --pipeline_config_path=PATH_TO_CONFIG.config --trained_checkpoint_prefix=PATH_TO_model.ckpt-XXXX --output_directory=PATH_TO_OUTPUT_FOLDER
```

Further optimisations on the frozen graph are possible, by quantizing, folding or even removing `batch_norm` from the graph, but I found that these steps hardly make a difference in my case.
See for example [this stackoverflow post](https://stackoverflow.com/questions/45382917/how-to-optimize-for-inference-a-simple-saved-tensorflow-1-0-1-graph).


### Resources

[This](https://jameslittle.me/blog/2019/tensorflow-object-detection) webpage alongside with the official [repo's instructions](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/running_locally.md) (plus [here](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md) for my dataset) helped me a lot in figuring out what's the *current* way of using the TF Object Detection API as a lot of other resources online are now outdated.

